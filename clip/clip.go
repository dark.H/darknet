package clip

import (
	"gitee.com/dark.H/dark"
	"golang.design/x/clipboard"
)

func Read() dark.Str {

	// Init returns an error if the package is not ready for use.
	err := clipboard.Init()
	if err != nil {
		panic(err)
	}
	return dark.Str(clipboard.Read(clipboard.FmtText))
}

func Write(content dark.Str) {

	// Init returns an error if the package is not ready for use.
	err := clipboard.Init()
	if err != nil {
		panic(err)
	}
	clipboard.Write(clipboard.FmtText, content.Bytes())
}
