package darknet

import (
	"net"

	"gitee.com/dark.H/dark"
	"gitee.com/dark.H/darknet/tcp"
)

func Connect(network dark.Str, dst dark.Str, do func(c net.Conn)) bool {
	var con net.Conn
	var err error
	if network == "tcp" {
		con, err = net.Dial("tcp", dst.Str())
		if err != nil {
			return false
		}
	} else if network.StartsWith("socks5://") {
		if dialer := tcp.Socks5Go(network); dialer == nil {
			return false
		} else {
			con, err = dialer.Dial("tcp", dst.Str())
			if err != nil {
				return false
			}
		}
	} else if network == "udp" {
		con, err = net.Dial("udp", dst.Str())
		if err != nil {
			return false
		}
	} else if network == "http" || network == "https" {

	}
	defer con.Close()
	do(con)
	return true
}

func TCPAlive() {

}
