package data

import "gitee.com/dark.H/dark"

func BatchAs(lines dark.Strs, joinBy dark.Str, batchSize int, factory func(no int, line dark.Str) dark.Str) <-chan dark.Str {
	ch := make(chan dark.Str)
	batches := dark.List[dark.Str]{}
	lines.Every(func(no int, i dark.Str) {

		if no%batchSize == 0 && no > 0 {
			e := batches.Join(joinBy.Str())
			ch <- e
			batches = dark.List[dark.Str]{}
		}
		o := factory(no, i)
		batches = batches.Add(o)

	})
	defer func() {
		close(ch)
	}()
	return ch
}
