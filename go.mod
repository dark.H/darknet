module gitee.com/dark.H/darknet

go 1.18

require (
	gitee.com/dark.H/dark v1.3.2
	github.com/google/uuid v1.3.0
	github.com/nsf/termbox-go v1.1.1
	github.com/pkg/sftp v1.13.5
	golang.design/x/clipboard v0.6.3
	golang.org/x/crypto v0.2.0
	golang.org/x/net v0.2.0
	golang.org/x/sync v0.1.0
	golang.org/x/sys v0.2.0
)

require (
	github.com/fatih/color v1.13.0 // indirect
	github.com/kr/fs v0.1.0 // indirect
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	golang.org/x/exp v0.0.0-20190731235908-ec7cb31e5a56 // indirect
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410 // indirect
	golang.org/x/mobile v0.0.0-20210716004757-34ab1303b554 // indirect
	golang.org/x/term v0.2.0 // indirect
)
