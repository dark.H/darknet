package main

import (
	"gitee.com/dark.H/dark"
	"gitee.com/dark.H/darknet/console"
)

func main() {

	// fmt.Println(console.GetWindowsSize())

	// console.Renderbar(func(width int) {
	// 	console.OnSignal(func(k termbox.Event) {
	// 		fmt.Println(k)
	// 	})
	// })

	// loc := &console.PanelLoc{
	// 	X: 2,
	// 	Y: 2,
	// }
	// panels := console.NewListPanel("G list", dark.List[dark.Str]{
	// 	"test",
	// 	"test2",
	// }, 20)

	// mode := 0
	// console.OnSignal(func() {
	// 	panels.Draw()
	// }, func(k termbox.Event) bool {
	// 	if mode == 0 {
	// 		if k.Ch == ':' {
	// 			mode = 1
	// 			if res, cancel := console.OnInput(loc, func(input dark.Str, key termbox.Event) {

	// 			}); !cancel {
	// 				panels.Add(res)
	// 				mode = 0
	// 				termbox.Init()

	// 				// fmt.Println(res.Bytes())

	// 			}
	// 		} else {
	// 			panels.OnKey(k)
	// 		}

	// 		return false
	// 	} else {
	// 		fmt.Println("gg")

	// 		return false
	// 	}

	// 	// return false

	// })

	panels := console.NewListPanel("G list", dark.List[dark.Str]{
		"[aes-256-cfb][mode:][Single][match.p4p.1688.com]",
		"[aes-256-cfb][mode:][Single][s2.ssl.qhres2.com]",
		"[aes-256-cfb][mode:][Single][p1.ssl.qhimg.com]",
		"[aes-256-cfb][mode:][Single][s3m4.fenxi.com]",
		"[aes-256-cfb][mode:][Single][ssxd.mediav.com]",
		"[aes-256-cfb][mode:][Single][p1.ssl.qhimgs3.com]",
		"[aes-256-cfb][mode:][Single][ssxd.mediav.com]",
		"[aes-256-cfb][mode:][Single][ssxd.mediav.com]",
		"[aes-256-cfb][mode:][Single][ssxd.mediav.com]",
		"[aes-256-cfb][mode:][Single][p2.ssl.qhimg.com]",
		"[aes-256-cfb][mode:][Single][ssxd.mediav.com]",
		"[aes-256-cfb][mode:][Single][s.ssl.qhres2.com]",
		"[aes-256-cfb][mode:][Single][p2.ssl.qhimg.com]",
		"[aes-256-cfb][mode:][Single][s3m2.fenxi.com]",
		"[aes-256-cfb][mode:][Single][s3m2.fenxi.com]",
		"[aes-256-cfb][mode:][Single][s3m2.fenxi.com]",
		"[aes-256-cfb][mode:][Single][ssxd.mediav.com]",
		"[aes-256-cfb][mode:][Single][s3m2.fenxi.com]",
		"[aes-256-cfb][mode:][Single][s3m4.fenxi.com]",
		"[aes-256-cfb][mode:][Single][s3m4.fenxi.com]",
		"[aes-256-cfb][mode:][Single][s3m4.fenxi.com]",
		"[aes-256-cfb][mode:][Single][s3m4.fenxi.com]",
		"[aes-256-cfb][mode:][Single][s3m4.fenxi.com]",
		"[aes-256-cfb][mode:][Single][lib.sinaapp.com]",
		"[aes-256-cfb][mode:][Single][show-g.mediav.com]",
		"[aes-256-cfb][mode:][Single][s3m.mediav.com]",
		"[aes-256-cfb][mode:][Single][max-l.mediav.com]",
		"[aes-256-cfb][mode:][Single][www.google.com]",
		"[aes-256-cfb][mode:][Single][www.google.com]",
		"[aes-256-cfb][mode:][Single][alive.github.com]",
		"[aes-256-cfb][mode:][Single][www.google.com]",
		"[aes-256-cfb][mode:][Single][nexus-websocket-a.intercom.io]",
		"[aes-256-cfb][mode:][Single][alive.github.com]",
		"[aes-256-cfb][mode:][Single][www.google.com]",
		"[aes-256-cfb][mode:][Single][alive.github.com]",
		"[aes-256-cfb][mode:][Single][nexus-websocket-a.intercom.io]",
		"[aes-256-cfb][mode:][Single][www.google.com]",
		"[aes-256-cfb][mode:][Single][nexus-websocket-a.intercom.io]",
		"[aes-256-cfb][mode:][Single][alive.github.com]",
		"[aes-256-cfb][mode:][Single][nexus-websocket-a.intercom.io]",
		"[aes-256-cfb][mode:][Single][alive.github.com]",
		"[aes-256-cfb][mode:][Single][alive.github.com]",
		"[aes-256-cfb][mode:][Single][nexus-websocket-a.intercom.io]",
		"[aes-256-cfb][mode:][Single][www.google.com]",
		"[aes-256-cfb][mode:][Single][alive.github.com]",
	}, 20)

	console.Renderbar(func(row, width int) {
		console.OnSignal(func() {
			panels.Draw()
		}, panels.OnKey)
	})

	app := console.NewApp()
	app.Regist("g", panels)
	app.Start()

	// .Color("g", "B", "F").Println("use this")
}
