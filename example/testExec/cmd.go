package main

import (
	"flag"

	"gitee.com/dark.H/dark"
)

func main() {

	flag.Parse()
	cmd := dark.List[string](flag.Args()).Join(" ")
	cmd.Exec("dark", "passdf").Println("output")
}
