package main

import (
	"fmt"

	"gitee.com/dark.H/dark"
	"gitee.com/dark.H/darknet"
	"gitee.com/dark.H/darknet/tcp"
)

func main() {

	// darknet.PingAlive("192.168.31.0/24").Every(func(no int, i dark.Str) {
	// 	i.ANSIGreen().Println()
	// })

	fmt.Println("---- test port -----")
	pool := darknet.NewPool(50)
	alives := dark.List[string]{}
	go pool.AsyncOutput(func(result any) {
		if result != nil {
			alives = append(alives, result.(dark.Str).Str())
		}
	})

	darknet.RangeIP("192.168.31.0/24").Every(func(ix int, i dark.Str) {
		// fmt.Println("test :", i+":22")
		pool.Async(func(no int64) any {
			// fmt.Println("test :", i+":22")
			if tcp.TestAlive("tcp", i+":80") {
				return i + ":80"
			}
			return nil
		})
	})

	pool.WaitStop()
	alives.Every(func(no int, i string) {
		dark.Str(i).Color("g", "B").Println("alive")
	})
	// n := 0
	// ds := dark.List[int]{}
	// for {
	// 	n += 1
	// 	if n == 200 {
	// 		fmt.Println("Wait ...")
	// 		// time.Sleep(10)
	// 		break

	// 	}
	// 	ds = ds.Add(n)
	// }
	// darknet.NewPool(60).Map(ds.AsAny(), func(no int64, i any) any {
	// 	if no%2 == 0 {
	// 		res := darknet.AsReq(dark.Str("https://www.basdfaidu.com").AsNetReq()).Go()
	// 		if res.Err != nil {
	// 			return dark.Str("[%s] \n%s").F(no, res.Err.Error()).ANSIRed()
	// 		}
	// 		return dark.Str("[%s] \n%s").F(no, res.Str).ANSIBlue()
	// 	} else {
	// 		res := darknet.AsReq(dark.Str("https://www.douasdfasdfban.com").AsNetReq()).Go()
	// 		if res.Err != nil {
	// 			return dark.Str("[%s] \n%s").F(no, res.Err.Error()).ANSIRed()
	// 		}
	// 		return dark.Str("[%s] \n%s").F(no, res.Str).ANSIBlue()
	// 	}
	// }, func(i any) {
	// 	fmt.Println(i)
	// })

	// pool.Stop()
}
