package main

import (
	"log"
	"time"

	"gitee.com/dark.H/dark"
	"gitee.com/dark.H/darknet"
)

func main() {
	tmp := `POST /mcp/pc/pcsearch HTTP/1.1
	Host: ug.baidu.com
	Cookie: BIDUPSID=A6DF8E5361702094680A45425EDB2358; PSTM=1667487910; BAIDUID=A6DF8E5361702094209A795B360661B2:FG=1; BA_HECTOR=0dah8la40k0l8ka12hak0k2f1hm7m561f; BAIDUID_BFESS=A6DF8E5361702094209A795B360661B2:FG=1; ZFY=:AZOQCWMUbctrEQG9rMHB1SyTk4TmCPHBZIjgGS6dQVE:C; BDRCVFR[S4-dAuiWMmn]=I67x6TjHwwYf0; delPer=0; PSINO=5; H_PS_PSSID=36551_37687_37584_36885_37486_36805_36789_37533_37500_26350; BDORZ=B490B5EBF6F3CD402E515D22BCDA1598
	Content-Length: 56
	Sec-Ch-Ua: "Chromium";v="107", "Not=A?Brand";v="24"
	Sec-Ch-Ua-Platform: "Windows"
	Sec-Ch-Ua-Mobile: ?0
	User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.5304.63 Safari/537.36
	Content-Type: application/json
	Accept: */*
	Origin: https://www.baidu.com
	Sec-Fetch-Site: same-site
	Sec-Fetch-Mode: cors
	Sec-Fetch-Dest: empty
	Referer: https://www.baidu.com/s?wd=%E5%B7%B4%E5%9F%BA%E6%96%AF%E5%9D%A6%E5%89%8D%E6%80%BB%E7%90%86%E9%81%AD%E6%9E%AA%E5%87%BB+%E9%80%81%E5%8C%BB%E7%94%BB%E9%9D%A2%E6%9B%9D%E5%85%89&sa=fyb_n_homepage&rsv_dl=fyb_n_homepage&from=super&cl=3&tn=baidutop10&fr=top1000&rsv_idx=2&hisfilter=1
	Accept-Encoding: gzip, deflate
	Accept-Language: zh-CN,zh;q=0.9
	Connection: close
	
	{"invoke_info":{"pos_1":[{}],"pos_2":[{}],"pos_3":[{}]}}`
	netStr := dark.Str(tmp).AsNetReq()
	req := darknet.AsReq(netStr)
	if err := req.Build(); err != nil {
		log.Fatal(err)
	}

	res := req.HTTPS().Go()
	if res.Err != nil {
		log.Fatal(res.Err)
	}
	res.Println()

	req2 := dark.Str(`SOME /yanghaitao5000/article/details/121417635 HTTP/1.1
	Host: blog.csddarknet.net
	Sec-Ch-Ua: "Chromium";v="107", "Not=A?Brand";v="24"
	Sec-Ch-Ua-Mobile: ?0
	Sec-Ch-Ua-Platform: "Windows"
	Upgrade-Insecure-Requests: 1
	User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.5304.63 Safari/537.36
	Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
	Sec-Fetch-Site: none
	Sec-Fetch-Mode: navigate
	Sec-Fetch-User: ?1
	Sec-Fetch-Dest: document
	Accept-Encoding: gzip, deflate
	Accept-Language: zh-CN,zh;q=0.9
	Connection: close
	`).AsNetReq()
	req2.ANSIYellow().Println()
	res2 := darknet.AsReq(req2).SetMethod("get").HTTPS().SetProxy("http://127.0.0.1:8080").Go()
	if res2.Err != nil {
		log.Fatal(res2.Err)
	}
	// res2.Println()
	body := res2.Body()
	// body.Find(`(\<\w+.+?\>)`).Sort(dark.SortLen).Every(func(no int, i dark.Str) {
	// 	end := "</" + i.Find(`\<(\w+)`)[0] + ">"
	// 	// end.Println(i)
	// 	body.ExtractRaw(i.Str(), end.Str()).Every(func(nx int, s dark.Str) {
	// 		s.Println(no, nx)
	// 	})

	// })
	st := time.Now()

	body2 := body
	body.XmlCssSelect("li>a [href]").Every(func(no int, i dark.Str) {
		body2 = body2.Replace(i.Str(), i.Color("g", "B", "U").Str())
	})

	body2.EndPrintln("USED ", time.Since(st))

}
