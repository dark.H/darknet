package main

import (
	"os"

	"gitee.com/dark.H/dark"
	"gitee.com/dark.H/darknet/tcp"
)

func main() {
	sshc := tcp.AsSSH(dark.Str(os.Args[1]), dark.Str(os.Args[2]))
	if sshc.Client != nil {
		sshc.Terminal()
	}
}
